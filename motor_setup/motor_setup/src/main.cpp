/* 
This is a test sketch for the Adafruit assembled Motor Shield for Arduino v2
It won't work with v1.x motor shields! Only for the v2's with built in PWM
control

For use with the Adafruit Motor Shield v2 
---->	http://www.adafruit.com/products/1438

Edited by Hugo A. Garcia to test 4 motors.
*/

#include <Arduino.h>
#include <Wire.h>
#include <Adafruit_MotorShield.h>

// Create the motor shield object with the default I2C address
Adafruit_MotorShield AFMS = Adafruit_MotorShield(); 
// Or, create it with a different I2C address (say for stacking)
// Adafruit_MotorShield AFMS = Adafruit_MotorShield(0x61); 

// Select which 'port' M1, M2, M3 or M4. In this case, M1
Adafruit_DCMotor *motor_M1 = AFMS.getMotor(1);
Adafruit_DCMotor *motor_M2 = AFMS.getMotor(2);
Adafruit_DCMotor *motor_M3 = AFMS.getMotor(3);
Adafruit_DCMotor *motor_M4 = AFMS.getMotor(4);
// You can also make another motor on port M2
//Adafruit_DCMotor *myOtherMotor = AFMS.getMotor(2);

void setup() {
  Serial.begin(115200);           // set up Serial library at 9600 bps
  Serial.println("Adafruit Motorshield v2 - DC Motor test!");

  AFMS.begin();  // create with the default frequency 1.6KHz
  //AFMS.begin(1000);  // OR with a different frequency, say 1KHz
  
  // Set the speed to start, from 0 (off) to 255 (max speed)
  motor_M1->setSpeed(150);
  motor_M2->setSpeed(150);
  motor_M3->setSpeed(150);
  motor_M4->setSpeed(150);
  motor_M1->run(FORWARD);
  motor_M2->run(FORWARD);
  motor_M3->run(FORWARD);
  motor_M4->run(FORWARD);
  // turn on motor
  motor_M1->run(RELEASE);
  motor_M2->run(RELEASE);
  motor_M3->run(RELEASE);
  motor_M4->run(RELEASE);
}

void loop() {
  uint8_t i;
  
  Serial.print("tick");

  motor_M1->run(FORWARD);
  motor_M2->run(FORWARD);
  motor_M3->run(FORWARD);
  motor_M4->run(FORWARD);
  for (i=0; i<255; i++) {
    motor_M1->setSpeed(i);  
    motor_M2->setSpeed(i);  
    motor_M3->setSpeed(i);  
    motor_M4->setSpeed(i);  
    delay(10);
  }
  for (i=255; i!=0; i--) {
    motor_M1->setSpeed(i);  
    motor_M2->setSpeed(i);  
    motor_M3->setSpeed(i);  
    motor_M4->setSpeed(i);  
    delay(10);
  }
  
  Serial.print("tock");

  motor_M1->run(BACKWARD);
  motor_M2->run(BACKWARD);
  motor_M3->run(BACKWARD);
  motor_M4->run(BACKWARD);
  for (i=0; i<255; i++) {
    motor_M1->setSpeed(i);  
    motor_M2->setSpeed(i);  
    motor_M3->setSpeed(i);  
    motor_M4->setSpeed(i);  
    delay(10);
  }
  for (i=255; i!=0; i--) {
    motor_M1->setSpeed(i);  
    motor_M2->setSpeed(i);  
    motor_M3->setSpeed(i);  
    motor_M4->setSpeed(i);  
    delay(10);
  }

  Serial.print("tech");
  motor_M1->run(RELEASE);
  motor_M2->run(RELEASE);
  motor_M3->run(RELEASE);
  motor_M4->run(RELEASE);
  delay(1000);
}